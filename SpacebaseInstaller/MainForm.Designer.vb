﻿'
' Created by SharpDevelop.
' User: ShuttupJerk
' Date: 26/09/2015
' Time: 2:04 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class MainForm
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
		Me.pictureBox1 = New System.Windows.Forms.PictureBox()
		Me.pictureBox2 = New System.Windows.Forms.PictureBox()
		Me.label1 = New System.Windows.Forms.Label()
		Me.label2 = New System.Windows.Forms.Label()
		Me.label3 = New System.Windows.Forms.Label()
		Me.btnInstall = New System.Windows.Forms.Button()
		Me.chkTandC = New System.Windows.Forms.CheckBox()
		Me.btnRollback = New System.Windows.Forms.Button()
		Me.textBox1 = New System.Windows.Forms.TextBox()
		Me.btnOpen = New System.Windows.Forms.Button()
		Me.label5 = New System.Windows.Forms.Label()
		Me.button1 = New System.Windows.Forms.Button()
		Me.progressBar = New System.Windows.Forms.ProgressBar()
		Me.btnReadMe = New System.Windows.Forms.Button()
		CType(Me.pictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
		CType(Me.pictureBox2,System.ComponentModel.ISupportInitialize).BeginInit
		Me.SuspendLayout
		'
		'pictureBox1
		'
		Me.pictureBox1.BackColor = System.Drawing.SystemColors.WindowText
		Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"),System.Drawing.Image)
		Me.pictureBox1.InitialImage = Nothing
		Me.pictureBox1.Location = New System.Drawing.Point(-1, 72)
		Me.pictureBox1.Name = "pictureBox1"
		Me.pictureBox1.Size = New System.Drawing.Size(140, 334)
		Me.pictureBox1.TabIndex = 0
		Me.pictureBox1.TabStop = false
		'
		'pictureBox2
		'
		Me.pictureBox2.BackColor = System.Drawing.SystemColors.InfoText
		Me.pictureBox2.Image = CType(resources.GetObject("pictureBox2.Image"),System.Drawing.Image)
		Me.pictureBox2.InitialImage = Nothing
		Me.pictureBox2.Location = New System.Drawing.Point(43, 3)
		Me.pictureBox2.Name = "pictureBox2"
		Me.pictureBox2.Size = New System.Drawing.Size(400, 66)
		Me.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
		Me.pictureBox2.TabIndex = 1
		Me.pictureBox2.TabStop = false
		'
		'label1
		'
		Me.label1.BackColor = System.Drawing.SystemColors.WindowText
		Me.label1.Location = New System.Drawing.Point(-1, -5)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(482, 77)
		Me.label1.TabIndex = 2
		'
		'label2
		'
		Me.label2.Location = New System.Drawing.Point(145, 138)
		Me.label2.Name = "label2"
		Me.label2.Size = New System.Drawing.Size(336, 58)
		Me.label2.TabIndex = 3
		Me.label2.Text = resources.GetString("label2.Text")
		'
		'label3
		'
		Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
		Me.label3.Location = New System.Drawing.Point(145, 72)
		Me.label3.Name = "label3"
		Me.label3.Size = New System.Drawing.Size(323, 66)
		Me.label3.TabIndex = 4
		Me.label3.Text = "Welcome to the Installer for the"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"SpaceBase DF9 "&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"Unofficial Patch 1.08.1"
		Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'btnInstall
		'
		Me.btnInstall.Enabled = false
		Me.btnInstall.Location = New System.Drawing.Point(351, 355)
		Me.btnInstall.Name = "btnInstall"
		Me.btnInstall.Size = New System.Drawing.Size(117, 42)
		Me.btnInstall.TabIndex = 6
		Me.btnInstall.Text = "Install"
		Me.btnInstall.UseVisualStyleBackColor = true
		AddHandler Me.btnInstall.Click, AddressOf Me.BtnInstallClick
		'
		'chkTandC
		'
		Me.chkTandC.Location = New System.Drawing.Point(145, 229)
		Me.chkTandC.Name = "chkTandC"
		Me.chkTandC.Size = New System.Drawing.Size(336, 30)
		Me.chkTandC.TabIndex = 7
		Me.chkTandC.Text = "I agree to the Terms and Conditions under the Licensing Agreement"
		Me.chkTandC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.chkTandC.UseVisualStyleBackColor = true
		AddHandler Me.chkTandC.CheckedChanged, AddressOf Me.ChkTandCCheckedChanged
		'
		'btnRollback
		'
		Me.btnRollback.Location = New System.Drawing.Point(145, 355)
		Me.btnRollback.Name = "btnRollback"
		Me.btnRollback.Size = New System.Drawing.Size(117, 42)
		Me.btnRollback.TabIndex = 8
		Me.btnRollback.Text = "Rollback to v1.06 (uninstall)"
		Me.btnRollback.UseVisualStyleBackColor = true
		AddHandler Me.btnRollback.Click, AddressOf Me.BtnRollbackClick
		'
		'textBox1
		'
		Me.textBox1.Location = New System.Drawing.Point(145, 320)
		Me.textBox1.Name = "textBox1"
		Me.textBox1.Size = New System.Drawing.Size(243, 20)
		Me.textBox1.TabIndex = 9
		'
		'btnOpen
		'
		Me.btnOpen.Location = New System.Drawing.Point(394, 320)
		Me.btnOpen.Name = "btnOpen"
		Me.btnOpen.Size = New System.Drawing.Size(74, 20)
		Me.btnOpen.TabIndex = 10
		Me.btnOpen.Text = "Open"
		Me.btnOpen.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.btnOpen.UseVisualStyleBackColor = true
		AddHandler Me.btnOpen.Click, AddressOf Me.BtnOpenClick
		'
		'label5
		'
		Me.label5.Location = New System.Drawing.Point(145, 302)
		Me.label5.Name = "label5"
		Me.label5.Size = New System.Drawing.Size(323, 15)
		Me.label5.TabIndex = 11
		Me.label5.Text = "Please Select the directory where Spacebase DF9 is installed:"
		'
		'button1
		'
		Me.button1.Location = New System.Drawing.Point(145, 196)
		Me.button1.Name = "button1"
		Me.button1.Size = New System.Drawing.Size(323, 23)
		Me.button1.TabIndex = 12
		Me.button1.Text = "Terms and Conditions"
		Me.button1.UseVisualStyleBackColor = true
		AddHandler Me.button1.Click, AddressOf Me.Button1Click
		'
		'progressBar
		'
		Me.progressBar.Location = New System.Drawing.Point(145, 265)
		Me.progressBar.Name = "progressBar"
		Me.progressBar.Size = New System.Drawing.Size(323, 23)
		Me.progressBar.TabIndex = 13
		'
		'btnReadMe
		'
		Me.btnReadMe.Location = New System.Drawing.Point(268, 355)
		Me.btnReadMe.Name = "btnReadMe"
		Me.btnReadMe.Size = New System.Drawing.Size(77, 42)
		Me.btnReadMe.TabIndex = 14
		Me.btnReadMe.Text = "READ ME!"
		Me.btnReadMe.UseVisualStyleBackColor = true
		AddHandler Me.btnReadMe.Click, AddressOf Me.BtnReadMeClick
		'
		'MainForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(480, 405)
		Me.Controls.Add(Me.btnReadMe)
		Me.Controls.Add(Me.progressBar)
		Me.Controls.Add(Me.button1)
		Me.Controls.Add(Me.label5)
		Me.Controls.Add(Me.btnOpen)
		Me.Controls.Add(Me.textBox1)
		Me.Controls.Add(Me.btnRollback)
		Me.Controls.Add(Me.chkTandC)
		Me.Controls.Add(Me.btnInstall)
		Me.Controls.Add(Me.label3)
		Me.Controls.Add(Me.label2)
		Me.Controls.Add(Me.pictureBox2)
		Me.Controls.Add(Me.label1)
		Me.Controls.Add(Me.pictureBox1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.Name = "MainForm"
		Me.Text = "Unofficial Spacebase DF9 Patch v1.08.1"
		CType(Me.pictureBox1,System.ComponentModel.ISupportInitialize).EndInit
		CType(Me.pictureBox2,System.ComponentModel.ISupportInitialize).EndInit
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private btnReadMe As System.Windows.Forms.Button
	Private progressBar As System.Windows.Forms.ProgressBar
	Private button1 As System.Windows.Forms.Button
	Private label5 As System.Windows.Forms.Label
	Private btnOpen As System.Windows.Forms.Button
	Private textBox1 As System.Windows.Forms.TextBox
	Private btnRollback As System.Windows.Forms.Button
	Private chkTandC As System.Windows.Forms.CheckBox
	Private btnInstall As System.Windows.Forms.Button
	Private label3 As System.Windows.Forms.Label
	Private label2 As System.Windows.Forms.Label
	Private label1 As System.Windows.Forms.Label
	Private pictureBox2 As System.Windows.Forms.PictureBox
	Private pictureBox1 As System.Windows.Forms.PictureBox
End Class
