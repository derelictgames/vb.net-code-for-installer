﻿'
' Created by SharpDevelop.
' User: ShuttupJerk
' Date: 26/09/2015
' Time: 3:27 PM
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Partial Class Form1
	Inherits System.Windows.Forms.Form
	
	''' <summary>
	''' Designer variable used to keep track of non-visual components.
	''' </summary>
	Private components As System.ComponentModel.IContainer
	
	''' <summary>
	''' Disposes resources used by the form.
	''' </summary>
	''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If components IsNot Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub
	
	''' <summary>
	''' This method is required for Windows Forms designer support.
	''' Do not change the method contents inside the source code editor. The Forms designer might
	''' not be able to load this method if it was changed manually.
	''' </summary>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
		Me.textBox1 = New System.Windows.Forms.TextBox()
		Me.label1 = New System.Windows.Forms.Label()
		Me.pictureBox1 = New System.Windows.Forms.PictureBox()
		Me.btnClose = New System.Windows.Forms.Button()
		CType(Me.pictureBox1,System.ComponentModel.ISupportInitialize).BeginInit
		Me.SuspendLayout
		'
		'textBox1
		'
		Me.textBox1.Location = New System.Drawing.Point(12, 72)
		Me.textBox1.Multiline = true
		Me.textBox1.Name = "textBox1"
		Me.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
		Me.textBox1.Size = New System.Drawing.Size(542, 198)
		Me.textBox1.TabIndex = 0
		Me.textBox1.Text = resources.GetString("textBox1.Text")
		'
		'label1
		'
		Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
		Me.label1.Location = New System.Drawing.Point(12, 9)
		Me.label1.Name = "label1"
		Me.label1.Size = New System.Drawing.Size(479, 60)
		Me.label1.TabIndex = 1
		Me.label1.Text = "Unofficial Spacebase DF9 Patch v1.08.1"&Global.Microsoft.VisualBasic.ChrW(13)&Global.Microsoft.VisualBasic.ChrW(10)&"Terms and Conditions"
		Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'pictureBox1
		'
		Me.pictureBox1.BackColor = System.Drawing.SystemColors.WindowText
		Me.pictureBox1.ErrorImage = CType(resources.GetObject("pictureBox1.ErrorImage"),System.Drawing.Image)
		Me.pictureBox1.Image = CType(resources.GetObject("pictureBox1.Image"),System.Drawing.Image)
		Me.pictureBox1.InitialImage = Nothing
		Me.pictureBox1.Location = New System.Drawing.Point(497, 12)
		Me.pictureBox1.Name = "pictureBox1"
		Me.pictureBox1.Size = New System.Drawing.Size(57, 51)
		Me.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
		Me.pictureBox1.TabIndex = 2
		Me.pictureBox1.TabStop = false
		'
		'btnClose
		'
		Me.btnClose.Location = New System.Drawing.Point(460, 276)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(94, 26)
		Me.btnClose.TabIndex = 3
		Me.btnClose.Text = "Close"
		Me.btnClose.UseVisualStyleBackColor = true
		AddHandler Me.btnClose.Click, AddressOf Me.BtnCloseClick
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(566, 308)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.pictureBox1)
		Me.Controls.Add(Me.label1)
		Me.Controls.Add(Me.textBox1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
		Me.Name = "Form1"
		Me.Text = "Unofficial Spacebase DF9 Patch v1.08.1"
		CType(Me.pictureBox1,System.ComponentModel.ISupportInitialize).EndInit
		Me.ResumeLayout(false)
		Me.PerformLayout
	End Sub
	Private btnClose As System.Windows.Forms.Button
	Private pictureBox1 As System.Windows.Forms.PictureBox
	Private label1 As System.Windows.Forms.Label
	Private textBox1 As System.Windows.Forms.TextBox
End Class
