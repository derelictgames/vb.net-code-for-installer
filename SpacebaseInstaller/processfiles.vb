﻿'
' Created by SharpDevelop.
' User: Rick Jones "Skenners"
' Date: 26/09/2015
' Time: 4:31 PM
'
' Last Update: 16/03/2016 by Rick Jones
' 
' the laziest module ever. very, very bad.
'
Imports System.IO
Imports System.Resources
Imports System.Reflection

Public Module processfiles
	Dim ThisPatchVer As String = "108"
	
	Function OrigDir() As String()
		Dim Dirs(29) As String
		Dirs(0) = ""
		Dirs(1) = "Data"
		Dirs(2) = "Mods"
		Dirs(3) = "Win"
		Dirs(4) = "Texture"
		Dirs(5) = "GFX"
		Dirs(6) = "Data\Dialog\Linecodes"
		Dirs(7) = "Data\UILayouts"
		Dirs(8) = "Data\Scripts"
		Dirs(9) = "Data\Scripts\Animations"
		Dirs(10) = "Data\Scripts\AnimEvents"
		Dirs(11) = "Data\Scripts\EffectEvents"
		Dirs(12) = "Data\Scripts\Environment"
		Dirs(13) = "Data\Scripts\EvnObjects"
		Dirs(14) = "Data\Scripts\Foods"
		Dirs(15) = "Data\Scripts\GameEvents"
		Dirs(16) = "Data\Scripts\Pickups"
		Dirs(17) = "Data\Scripts\PostFX"
		Dirs(18) = "Data\Scripts\SeqCommands"
		Dirs(19) = "Data\Scripts\String"
		Dirs(20) = "Data\Scripts\Tools"
		Dirs(21) = "Data\Scripts\Tools\GeoEdit"
		Dirs(22) = "Data\Scripts\Tools\ParticleEdit"
		Dirs(23) = "Data\Scripts\UI\Data"
		Dirs(24) = "Data\Scripts\UI"
		Dirs(25) = "Data\Scripts\Utility\Tasks"
		Dirs(26) = "Data\Scripts\Utility"
		Dirs(27) = "Data\Scripts\WorldObjects"
		Dirs(28) = "Data\Scripts\Zones"
		Dirs(29) = "Data\Common\DFCommon"

		Return Dirs
	End Function
	
	Function NameArray() As String(,)
		
		Dim files(173,2) As String
		files(0,0) = "Data\Scripts\Utility\ActivityOption.lua" 'filename
		files(0,1) = "byte"	'resource type
		files(0,2) = "ActivityOption" 'resource name
		files(1,0) = "Data\Scripts\Utility\Tasks\AttackEnemy.lua"
		files(1,1) = "byte"	
		files(1,2) = "AttackEnemy"
		files(2,0) = "Data\Scripts\UI\AudioVideoSettings.lua"
		files(2,1) = "byte"	
		files(2,2) = "AudioVideoSettings"
		files(3,0) = "Data\UILayouts\AudioVideoSettingsLayout.lua"
		files(3,1) = "byte"	
		files(3,2) = "AudioVideoSettingsLayout"
		files(4,0) = "Data\Scripts\Base.lua"
		files(4,1) = "byte"
		files(4,2) = "Base"
		files(5,0) = "Win\Munged\UI\BeaconMenu.lua"
		files(5,1) = "byte"	
		files(5,2) = "BeaconMenu_1"
		files(6,0) = "Data\Scripts\UI\BeaconMenu.lua"
		files(6,1) = "byte"	
		files(6,2) = "BeaconMenu"
		files(7,0) = "Data\Scripts\UI\BeaconMenuEdit.lua"
		files(7,1) = "byte"	
		files(7,2) = "BeaconMenuEdit"
		files(8,0) = "Data\UILayouts\BeaconMenuEditLayout.lua"
		files(8,1) = "byte"	
		files(8,2) = "BeaconMenuEditLayout" 
		files(9,0) = "Data\Scripts\UI\BeaconMenuEntry.lua"
		files(9,1) = "byte"	
		files(9,2) = "BeaconMenuEntry"		
		files(10,0) = "Data\UILayouts\BeaconMenuEntryLayout.lua"
		files(10,1) = "byte"
		files(10,2) = "BeaconMenuEntryLayout"
		files(11,0) = "Data\UILayouts\BeaconMenuLayout.lua"	
		files(11,1) = "byte"	
		files(11,2) = "BeaconMenuLayout"
		files(12,0) = "Data\Scripts\GameEvents\BreachingEvent.lua"
		files(12,1) = "byte"	
		files(12,2) = "BreachingEvent"
		files(13,0) = "Data\build.string"
		files(13,1) = "byte"
		files(13,2) = "build"
		files(14,0) = "Data\Scripts\UI\BuildHelper.lua"
		files(14,1) = "byte"
		files(14,2) = "BuildHelper"
		files(15,0) = "Win\Munged\Environments\bulbous_plant.lua"
		files(15,1) = "byte"	
		files(15,2) = "bulbous_plant"		
		files(16,0) = "Win\Munged\Environments\bulbus_plant.png"
		files(16,1) = "bmp"	
		files(16,2) = "bulbus_plant"		
		files(17,0) = "Data\Scripts\Character.lua"
		files(17,1) = "byte"
		files(17,2) = "Character"
		files(18,0) = "Data\Scripts\CharacterConstants.lua"
		files(18,1) = "byte"
		files(18,2) = "CharacterConstants"
		files(19,0) = "Data\Scripts\CharacterManager.lua"
		files(19,1) = "byte"
		files(19,2) = "CharacterManager"
		files(20,0) = "Data\Scripts\Utility\Tasks\CircleBeacon.lua"
		files(20,1) = "byte"	
		files(20,2) = "CircleBeacon"
		files(21,0) = "Data\Scripts\UI\CitizenActionTab.lua"
		files(21,1) = "byte"	
		files(21,2) = "CitizenActionTab"
		files(22,0) = "Data\Scripts\UI\CitizenDutyButton.lua"
		files(22,1) = "byte"	
		files(22,2) = "CitizenDutyButton"				
		files(23,0) = "Data\UILayouts\CitizenDutyButtonLayout.lua"
		files(23,1) = "byte"	
		files(23,2) = "CitizenDutyButtonLayout"
		files(24,0) = "Data\Scripts\UI\CitizenDutyTab.lua"
		files(24,1) = "byte"	
		files(24,2) = "CitizenDutyTab"
		files(25,0) = "Data\UILayouts\CitizenDutyTabLayout.lua"
		files(25,1) = "byte"	
		files(25,2) = "CitizenDutyTabLayout"
		files(26,0) = "Data\Scripts\UI\CitizenInspector.lua"
		files(26,1) = "byte"	
		files(26,2) = "CitizenInspector"		
		files(27,0) = "Data\Scripts\CitizenNames.lua"
		files(27,1) = "byte"	
		files(27,2) = "CitizenNames"	
		files(28,0) = "Data\Scripts\UI\CitizenStatsTab.lua"
		files(28,1) = "byte"	
		files(28,2) = "CitizenStatsTab"	
		files(29,0) = "Data\Scripts\Utility\Tasks\Clean.lua"
		files(29,1) = "byte"	
		files(29,2) = "Clean"
		files(30,0) = "Data\Scripts\GameEvents\CompoundEvent.lua"
		files(30,1) = "byte"	
		files(30,2) = "CompoundEvent"
		files(31,0) = "Data\Scripts\config.lua"
		files(31,1) = "byte"
		files(31,2) = "config"
		files(32,0) = "Data\Scripts\UI\ConstructMenu.lua"
		files(32,1) = "byte"
		files(32,2) = "ConstructMenu"
		files(33,0) = "COPYING"
		files(33,1) = "byte"
		files(33,2) = "COPYING"	
		files(34,0) = "Data\UILayouts\CreditsLayout.lua"
		files(34,1) = "byte"	
		files(34,2) = "CreditsLayout"
		files(35,0) = "Data\Common\DFCommon\DataCache.lua"
		files(35,1) = "byte"	
		files(35,2) = "DataCache"
		files(36,0) = "Data\Scripts\UI\DebugMenu.lua"
		files(36,1) = "byte"	
		files(36,2) = "DebugMenu"
		files(37,0) = "Data\UILayouts\DebugMenuLayout.lua"
		files(37,1) = "byte"	
		files(37,2) = "DebugMenuLayout"
		files(38,0) = "Data\Scripts\GameEvents\DerelictEvent.lua"
		files(38,1) = "byte"	
		files(38,2) = "DerelictEvent"
		files(39,0) = "Texture\Dgl.png"
		files(39,1) = "bmp"	
		files(39,2) = "Dgl"		
		files(40,0) = "Data\Scripts\Docking.lua"
		files(40,1) = "byte"	
		files(40,2) = "Docking"
		files(41,0) = "Data\Scripts\GameEvents\DockingEvent.lua"
		files(41,1) = "byte"	
		files(41,2) = "DockingEvent"
		files(42,0) = "Data\UILayouts\DockingRequestLayout.lua"
		files(42,1) = "byte"	
		files(42,2) = "DockingRequestLayout"
		files(43,0) = "Data\UILayouts\DockingResponseLayout.lua"
		files(43,1) = "byte"	
		files(43,2) = "DockingResponseLayout"
		files(44,0) = "Data\Scripts\EnvObjects\Door.lua"
		files(44,1) = "byte"	
		files(44,2) = "Door"
		files(45,0) = "Data\Scripts\Utility\Tasks\DropOffCorpse.lua"
		files(45,1) = "byte"	
		files(45,2) = "DropOffCorpse"
		files(46,0) = "Data\Scripts\Utility\EmergencyBeacon.lua"
		files(46,1) = "byte"	
		files(46,2) = "EmergencyBeacon"
		files(47,0) = "Data\Scripts\EnvObjects\EnvObject.lua"
		files(47,1) = "byte"	
		files(47,2) = "EnvObject"
		files(48,0) = "Data\Scripts\EnvObjects\EnvObjectData.lua"
		files(48,1) = "byte"	
		files(48,2) = "EnvObjectData"
		files(49,0) = "Data\Scripts\ErrorReporting.lua"
		files(49,1) = "byte"	
		files(49,2) = "ErrorReporting"
		files(50,0) = "Data\Scripts\GameEvents\Event.lua"
		files(50,1) = "byte"	
		files(50,2) = "Event"
		files(51,0) = "Data\Scripts\EventController.lua"
		files(51,1) = "byte"	
		files(51,2) = "EventController"
		files(52,0) = "Data\Scripts\GameEvents\EventData.lua"
		files(52,1) = "byte"	
		files(52,2) = "EventData"
		files(53,0) = "Data\Scripts\Utility\Tasks\Explore.lua"
		files(53,1) = "byte"
		files(53,2) = "Explore"
		files(54,0) = "Data\Scripts\GameRules.lua"
		files(54,1) = "byte"
		files(54,2) = "GameRules"
		files(55,0) = "Data\Scripts\Utility\GlobalObjects.lua"
		files(55,1) = "byte"
		files(55,2) = "GlobalObjects"
		files(56,0) = "Data\Scripts\Goal.lua"
		files(56,1) = "byte"
		files(56,2) = "Goal"
		files(57,0) = "Data\Scripts\GoalData.lua"
		files(57,1) = "byte"
		files(57,2) = "GoalData"
		files(58,0) = "Data\Scripts\UI\Gui.lua"
		files(58,1) = "byte"
		files(58,2) = "Gui"
		files(59,0) = "Data\Scripts\UI\GuiManager.lua"
		files(59,1) = "byte"	
		files(59,2) = "GuiManager"
		files(60,0) = "Win\Munged\Environments\happybot.png"
		files(60,1) = "bmp"	
		files(60,2) = "happybot"
		files(61,0) = "Data\Scripts\EnvObjects\HappyBot.lua"
		files(61,1) = "byte"	
		files(61,2) = "HappyBot_2"
		files(62,0) = "Win\Munged\Environments\HappyBot.lua"
		files(62,1) = "byte"	
		files(62,2) = "HappyBot_1"
		files(63,0) = "Data\Scripts\HintChecks.lua"	
		files(63,1) = "byte"	
		files(63,2) = "HintChecks"
		files(64,0) = "Data\Scripts\HintData.lua"
		files(64,1) = "byte"	
		files(64,2) = "HintData"
		files(65,0) = "Data\Scripts\GameEvents\HostileDerelictEvent.lua"
		files(65,1) = "byte"	
		files(65,2) = "HostileDerelictEvent"
		files(66,0) = "Data\Scripts\GameEvents\HostileDockingEvent.lua"
		files(66,1) = "byte"	
		files(66,2) = "HostileDockingEvent"
		files(67,0) = "Data\Scripts\GameEvents\HostileImmigrationEvent.lua"
		files(67,1) = "byte"	
		files(67,2) = "HostileImmigrationEvent"
		files(68,0) = "Data\Scripts\GameEvents\ImmigrationEvent.lua"
		files(68,1) = "byte"	
		files(68,2) = "ImmigrationEvent"
		files(69,0) = "Data\Scripts\Utility\Tasks\Incapacitated.lua"
		files(69,1) = "byte"	
		files(69,2) = "Incapacitated"
		files(70,0) = "Win\Munged\UI\Inspector.lua"
		files(70,1) = "byte"	
		files(70,2) = "Inspector"
		files(71,0) = "InstalledPatches"
		files(71,1) = "byte"
		files(71,2) = "InstalledPatches"	
		files(72,0) = "Data\Scripts\Inventory.lua"
		files(72,1) = "byte"	
		files(72,2) = "Inventory"	
		files(73,0) = "Data\Scripts\InventoryData.lua"
		files(73,1) = "byte"	
		files(73,2) = "InventoryData"
		files(74,0) = "Win\Munged\UI\janitoricon.png"
		files(74,1) = "bmp"	
		files(74,2) = "janitoricon"			
		files(75,0) = "Win\Munged\UI\JanitorIcon.lua"
		files(75,1) = "byte"	
		files(75,2) = "JanitorIcon_1"	
		files(76,0) = "Data\Scripts\UI\JobAssignment.lua"
		files(76,1) = "byte"
		files(76,2) = "JobAssignment"
		files(77,0) = "Data\Scripts\UI\JobRoster.lua"
		files(77,1) = "byte"	
		files(77,2) = "JobRoster"
		files(78,0) = "Data\Scripts\UI\JobRosterEntry.lua"
		files(78,1) = "byte"	
		files(78,2) = "JobRosterEntry"
		files(79,0) = "Data\UILayouts\JobRosterEntryLayout.lua"
		files(79,1) = "byte"	
		files(79,2) = "JobRosterEntryLayout"
		files(80,0) = "Data\UILayouts\JobRosterLayout.lua"
		files(80,1) = "byte"	
		files(80,2) = "JobRosterLayout"
		files(81,0) = "Data\Scripts\EnvObjects\Jukebox.lua"
		files(81,1) = "byte"	
		files(81,2) = "Jukebox"
		files(82,0) = "Win\Munged\Environments\Jukebox.lua"
		files(82,1) = "byte"	
		files(82,2) = "Jukebox_1"
		files(83,0) = "Win\Munged\Environments\Jukebox.png"
		files(83,1) = "bmp"
		files(83,2) = "Jukebox_2"
		files(84,0) = "Data\Scripts\Lighting.lua"
		files(84,1) = "byte"	
		files(84,2) = "Lighting"
		files(85,0) = "Data\Scripts\LinecodeManager.lua"
		files(85,1) = "byte"	
		files(85,2) = "LinecodeManager"
		files(86,0) = "Data\Scripts\Utility\Tasks\ListenToJukebox.lua"
		files(86,1) = "byte"	
		files(86,2) = "ListenToJukebox"
		files(87,0) = "Data\Scripts\UI\LoadSave.lua"
		files(87,1) = "byte"	
		files(87,2) = "LoadSave"
		files(88,0) = "Data\UILayouts\LoadSaveLayout.lua"
		files(88,1) = "byte"	
		files(88,2) = "LoadSaveLayout"
		files(89,0) = "Data\Scripts\Log.lua"
		files(89,1) = "byte"	
		files(89,2) = "Log"
		files(90,0) = "Data\Scripts\LuaGrid.lua"
		files(90,1) = "byte"	
		files(90,2) = "LuaGrid"
		files(91,0) = "Data\Scripts\main.lua"
		files(91,1) = "byte"	
		files(91,2) = "main"
		files(92,0) = "Data\Dialog\Linecodes\MainGame_enUS.lua"
		files(92,1) = "byte"
		files(92,2) = "MainGame_enUS"
		files(93,0) = "Data\Scripts\Malady.lua"
		files(93,1) = "byte"	
		files(93,2) = "Malady"
		files(94,0) = "Win\Munged\UI\MATT_White.png"
		files(94,1) = "bmp"	
		files(94,2) = "MATT_White"
		files(95,0) = "Data\Scripts\UI\MenuManager.lua"
		files(95,1) = "byte"	
		files(95,2) = "MenuManager"
		files(96,0) = "Data\Scripts\GameEvents\MeteorEvent.lua"
		files(96,1) = "byte"	
		files(96,2) = "MeteorEvent"
		files(97,0) = "Data\Scripts\SBRS\MOAIImageExt.lua"
		files(97,1) = "byte"
		files(97,2) = "MOAIImageExt"
		files(98,0) = "Data\Scripts\SBRS\MOAIPropExt.lua"
		files(98,1) = "byte"
		files(98,2) = "MOAIPropExt"
		files(99,0) = "Data\Scripts\MoneyCredits.lua"
		files(99,1) = "byte"
		files(99,2) = "MoneyCredits"
		files(100,0) = "Data\UILayouts\motd-client.json"
		files(100,1) = "byte"
		files(100,2) = "motd-client"	
		files(101,0) = "Data\Scripts\UI\NewBaseInspector.lua"
		files(101,1) = "byte"	
		files(101,2) = "NewBaseInspector"
		files(102,0) = "Data\UILayouts\NewBaseInspectorLayout.lua"
		files(102,1) = "byte"	
		files(102,2) = "NewBaseInspectorLayout"		
		files(103,0) = "Data\Scripts\UI\NewInspectMenu.lua"
		files(103,1) = "byte"	
		files(103,2) = "NewInspectMenu"		
		files(104,0) = "Data\Scripts\NewMaladyData.lua"
		files(104,1) = "byte"	
		files(104,2) = "NewMaladyData"		
		files(105,0) = "Data\Scripts\UI\NewSideBar.lua"
		files(105,1) = "byte"	
		files(105,2) = "NewSideBar"
		files(106,0) = "Win\Munged\Environments\O2Gen.lua"
		files(106,1) = "byte"
		files(106,2) = "O2Gen"
		files(107,0) = "Win\Munged\Environments\O2Gen3.lua"
		files(107,1) = "byte"
		files(107,2) = "O2Gen3"
		files(108,0) = "Win\Munged\Environments\O2Gen3.png"
		files(108,1) = "bmp"
		files(108,2) = "O2Gen3_1"
		files(109,0) = "Win\Munged\Environments\O2Gen4.lua"
		files(109,1) = "byte"
		files(109,2) = "O2Gen4"
		files(110,0) = "Win\Munged\Environments\O2Gen4.png"
		files(110,1) = "bmp"
		files(110,2) = "O2Gen4_1"
		files(111,0) = "Data\Scripts\UI\ObjectActionTab.lua"
		files(111,1) = "byte"	
		files(111,2) = "ObjectActionTab"
		files(112,0) = "Data\Scripts\ObjectList.lua"
		files(112,1) = "byte"	
		files(112,2) = "ObjectList"
		files(113,0) = "Data\Scripts\UI\ObjectMenu.lua"
		files(113,1) = "byte"	
		files(113,2) = "ObjectMenu"
		files(114,0) = "Data\UILayouts\ObjectMenuLayout.lua"
		files(114,1) = "byte"	
		files(114,2) = "ObjectMenuLayout"
		files(115,0) = "Win\Munged\Environments\Objects.lua"
		files(115,1) = "byte"	
		files(115,2) = "Objects"						
		files(116,0) = "Data\Scripts\Utility\OptionData.lua"
		files(116,1) = "byte"	
		files(116,2) = "OptionData"
		files(117,0) = "Data\Scripts\Pathfinder.lua"
		files(117,1) = "byte"	
		files(117,2) = "Pathfinder"
		files(118,0) = "Data\Scripts\Pickups\PickupData.lua"
		files(118,1) = "byte"	
		files(118,2) = "PickupData"
		files(119,0) = "Data\Scripts\PostFX\Post.lua"
		files(119,1) = "byte"	
		files(119,2) = "Post"
		files(120,0) = "Win\Munged\Environments\ReactorGen3.lua"
		files(120,1) = "byte"
		files(120,2) = "ReactorGen3"
		files(121,0) = "Win\Munged\Environments\ReactorGen3.png"
		files(121,1) = "bmp"
		files(121,2) = "ReactorGen3_1"
		files(122,0) = "Win\Munged\Environments\ReactorGen4.lua"
		files(122,1) = "byte"
		files(122,2) = "ReactorGen4"
		files(123,0) = "Win\Munged\Environments\ReactorGen4.png"
		files(123,1) = "bmp"
		files(123,2) = "ReactorGen4_1"
		files(124,0) = "README" 
		files(124,1) = "byte" 
		files(124,2) = "README" 
		files(125,0) = "Data\Scripts\EnvObjects\RefineryDropoff.lua"
		files(125,1) = "byte"	
		files(125,2) = "RefineryDropoff"
		files(126,0) = "Data\Scripts\UI\ResearchAssignment.lua"
		files(126,1) = "byte"
		files(126,2) = "ResearchAssignment"
		files(127,0) = "Data\Scripts\ResearchData.lua"
		files(127,1) = "byte"	
		files(127,2) = "ResearchData"
		files(128,0) = "Data\Scripts\UI\ResearchProjectEntry.lua"
		files(128,1) = "byte"	
		files(128,2) = "ResearchProjectEntry"
		files(129,0) = "Data\Scripts\UI\ResearchZoneEntry.lua"
		files(129,1) = "byte"	
		files(129,2) = "ResearchZoneEntry"
		files(130,0) = "Data\Scripts\Rig.lua"
		files(130,1) = "byte"	
		files(130,2) = "Rig"
		files(131,0) = "Data\Scripts\Pickups\Rock.lua"
		files(131,1) = "byte"	
		files(131,2) = "Rock"		
		files(132,0) = "Data\Scripts\Room.lua"
		files(132,1) = "byte"	
		files(132,2) = "Room"
		files(133,0) = "Data\Scripts\UI\ScrollableUI.lua"
		files(133,1) = "byte"	
		files(133,2) = "ScrollableUI"
		files(134,0) = "Data\Scripts\UI\SelectObjectForZoneMenu.lua"
		files(134,1) = "byte"	
		files(134,2) = "SelectObjectForZoneMenu"
		
		
		files(135,0) = "Data\UILayouts\SideBarLayout.lua"
		files(135,1) = "byte"	
		files(135,2) = "SideBarLayout"
		files(136,0) = "Data\Scripts\SoundManager.lua"
		files(136,1) = "byte"
		files(136,2) = "SoundManager"
		files(137,0) = "SpacebaseDF9\Saves\SpacebaseDF9AutoSave.sav"
		files(137,1) = "SAVEGAME"
		files(137,2) = "SpacebaseDF9AutoSave"
		files(138,0) = "Win\Audio\SpaceBaseV2\SpaceBaseV2.fev"
		files(138,1) = "byte"
		files(138,2) = "SpaceBaseV2"	
		files(139,0) = "Win\Audio\SpaceBaseV2\SpaceBaseV2.fsb"
		files(139,1) = "byte"	
		files(139,2) = "SpaceBaseV2_1"
		files(140,0) = "Data\Scripts\Squad.lua"
		files(140,1) = "byte"	
		files(140,2) = "Squad"
		files(141,0) = "Data\Scripts\UI\SquadEditEntry.lua"
		files(141,1) = "byte"	
		files(141,2) = "SquadEditEntry"
		files(142,0) = "Data\UILayouts\SquadEditEntryLayout.lua"
		files(142,1) = "byte"	
		files(142,2) = "SquadEditEntryLayout"
		files(143,0) = "Data\UILayouts\SquadEditLayout.lua"
		files(143,1) = "byte"	
		files(143,2) = "SquadEditLayout"
		files(144,0) = "Data\Scripts\UI\SquadEditMenu.lua"
		files(144,1) = "byte"	
		files(144,2) = "SquadEditMenu"
		files(145,0) = "Data\Scripts\UI\SquadEntry.lua"
		files(145,1) = "byte"	
		files(145,2) = "SquadEntry"
		files(146,0) = "Data\UILayouts\SquadEntryLayout.lua"
		files(146,1) = "byte"
		files(146,2) = "SquadEntryLayout"
		files(147,0) = "Data\Scripts\SquadList.lua"
		files(147,1) = "byte"	
		files(147,2) = "SquadList"
		files(148,0) = "Data\Scripts\UI\StartMenu.lua"
		files(148,1) = "byte"	
		files(148,2) = "StartMenu"
		files(149,0) = "Data\UILayouts\StartMenuLayout.lua"
		files(149,1) = "byte"	
		files(149,2) = "StartMenuLayout"
		files(150,0) = "Data\Scripts\UI\StatusBar.lua"
		files(150,1) = "byte"
		files(150,2) = "StatusBar"
		files(151,0) = "Data\UILayouts\StatusBarLayout.lua"
		files(151,1) = "byte"
		files(151,2) = "StatusBarLayout"
		files(152,0) = "Data\Scripts\Trade.lua"
		files(152,1) = "byte"	
		files(152,2) = "Trade"
		files(153,0) = "Data\UILayouts\TradeLayout.lua"
		files(153,1) = "byte"	
		files(153,2) = "TradeLayout"
		files(154,0) = "Data\Scripts\UI\TradeMenu.lua"
		files(154,1) = "byte"	
		files(154,2) = "TradeMenu"
		files(155,0) = "Data\Scripts\GameEvents\TraderEvent.lua"
		files(155,1) = "byte"	
		files(155,2) = "TraderEvent"
		files(156,0) = "Data\Scripts\UI\UIButton.lua"
		files(156,1) = "byte"	
		files(156,2) = "UIButton"
		files(157,0) = "Data\UILayouts\UIButtonLayout.lua"
		files(157,1) = "byte"	
		files(157,2) = "UIButtonLayout"		
		files(158,0) = "Data\Scripts\UI\UIElement.lua"
		files(158,1) = "byte"	
		files(158,2) = "UIElement"
		files(159,0) = "Data\Scripts\SBRS\UIElementModel.lua"
		files(159,1) = "byte"	
		files(159,2) = "UIElementModel"
		files(160,0) = "Data\Scripts\SBRS\UIList.lua"
		files(160,1) = "byte"	
		files(160,2) = "UIList"
		files(161,0) = "Data\Scripts\SBRS\UIScroll.lua"
		files(161,1) = "byte"	
		files(161,2) = "UIScroll"
		files(162,0) = "Data\Scripts\WeaponData.lua"
		files(162,1) = "byte"	
		files(162,2) = "WeaponData"
		files(163,0) = "Data\Scripts\World.lua"
		files(163,1) = "byte"	
		files(163,2) = "World"
		files(164,0) = "Data\Scripts\UI\WorldToolTip.lua"
		files(164,1) = "byte"	
		files(164,2) = "WorldToolTip"		
		files(165,0) = "Data\Scripts\Zones\Zone.lua"
		files(165,1) = "byte"	
		files(165,2) = "Zone"
		files(166,0) = "Data\Scripts\UI\ZoneActionTab.lua"
		files(166,1) = "byte"	
		files(166,2) = "ZoneActionTab"
		files(167,0) = "Data\UILayouts\ZoneResearchButtonLayout.lua"
		files(167,1) = "byte"	
		files(167,2) = "ZoneResearchButtonLayout"		
		files(168,0) = "Data\Scripts\UI\ZoneResearchPane.lua"
		files(168,1) = "byte"	
		files(168,2) = "ZoneResearchPane"				
		files(169,0) = "Data\Scripts\UI\ZoneRezoneTab.lua"
		files(169,1) = "byte"	
		files(169,2) = "ZoneRezoneTab"
		

		'removed list
		files(170,0) = "Data\Scripts\UI\SquadMenu.lua"
		files(170,1) = "REMOVED"	
		files(170,2) = "SquadMenu"
		files(171,0) = "Data\UILayouts\SquadLayout.lua"
		files(171,1) = "REMOVED"
		files(171,2) = "SquadLayout"
		files(172,0) = "Data\Scripts\DialogSets.lua"
		files(172,1) = "REMOVED"
		files(172,2) = "DialogSets"
		files(173,0) = "Data\Scripts\MaladyData.lua"
		files(173,1) = "REMOVED"	
		files(173,2) = "MaladyData"
		

		return files
	End Function
	
	Public Function PatchPrelim(appdir As String ) As Boolean
		If File.Exists(appdir + "\space.exe") <> True Then
			MsgBox("Spacebase Directory Not Found. Please Check the path again!")
			Return False
			Exit Function
		End If
		
		If Directory.Exists(appdir + "\PatchBackup") = false Then
			Directory.CreateDirectory(appdir + "\PatchBackup")
		End If
		
		
		If Directory.Exists(appdir + "\PatchBackup\v106") = True Then
			MsgBox("Please Rollback to v1.06 before installing this Patch." + vbCrLf + _
					   "If you have, delete the '<spacebase dir>\PatchBackup\v106' directory and try again")
			Return false
			Exit Function	
		Else
			Directory.CreateDirectory(appdir + "\PatchBackup\v106")
		End If
		
		Return True
		
	End function
		
	Public Function RunFileBackup(appdir As String) As Boolean
		Dim loopvar As Integer

		For loopvar = LBound(NameArray) To UBound(NameArray)
			If NameArray(loopvar,1) <> "SAVEGAME" Then
				If File.Exists(appdir + "\" + NameArray(loopvar,0)) = True Then
					If NameArray(loopvar,0).Contains("\") = True Then 
							If Directory.Exists(appdir + "\PatchBackup\v106" + Path.GetDirectoryName(NameArray(loopvar,0))) = False Then
								Dim directories() As String
								Dim i As Integer
								Dim dir_path As String
								
								dir_path = appdir + "\PatchBackup\v106\"
								directories = Split(NameArray(loopvar,0), "\")
								
							    For i = LBound(directories) To UBound(directories)-1
							    	If Len(directories(i)) > 0 Then
							            dir_path = dir_path & directories(i) & "\"
							            If Directory.Exists(dir_path) <> True Then
							            	Directory.CreateDirectory(dir_path)
							            End If
							    	End If
							    Next i
							End If
					
						File.Move(appdir + "\" + NameArray(loopvar,0), appdir + "\PatchBackup\v106\" + NameArray(loopvar,0)) 
					End If
	
				End If
			End if
		Next loopvar
		Return True
	End Function
	
	Public Function CopyPatch(appdir As String) As Boolean
		Dim resourceManager As New ResourceManager("SpacebaseInstaller.patchfiles", Assembly.GetExecutingAssembly)
		Dim result As Byte()
		Dim result2 As bitmap
		
		Dim i As Integer = 0
		
		For i = LBound(NameArray) To UBound(NameArray)
			Dim directories() As String
			Dim i2 As Integer
			Dim dir_path As String
			
			dir_path = appdir + "\"
			directories = Split(NameArray(i,0), "\")
			
			If NameArray(i,1) <> "SAVEGAME" Then
			    For i2 = LBound(directories) To UBound(directories)-1
			        If Len(directories(i2)) > 0 Then
			            dir_path = dir_path & directories(i2) & "\"
			            If Directory.Exists(dir_path) = false Then
			            	Directory.CreateDirectory(dir_path)
			            End If
			        End If
			    Next i2
			    
		    'if the file exists, its probably from a previous patch. So delete it and we replace it after with the new one
		    
		    
			    If File.Exists(appdir & "\" & NameArray(i,0)) = True Then
			    	File.Delete(appdir & "\" & NameArray(i,0))	
			    End If
	    
			    
				If NameArray(i,1) <> "REMOVED" Then    	
					If NameArray(i,1) = "byte" Then
						result = resourceManager.getobject(NameArray(i,2))
						File.WriteAllBytes(appdir & "\" & NameArray(i,0), result)
					End If
					If NameArray(i,1) = "bmp" Then
						result2 = resourceManager.getobject(Namearray(i,2))
						result2.Save(appdir & "\" & NameArray(i,0), System.Drawing.Imaging.ImageFormat.Png)
					End If
				End If
			End if
		Next i
		
		
		
		Return True
	End Function
	
	Public Function RollbackPrelim(appdir As String) As Boolean
		If MsgBox("Please Be Aware that Rollback Will Delete any Saved Files." &vbCrlf & "Please make a backup!",MsgBoxStyle.OkCancel) = vbCancel Then
			Return False
			Exit Function
		End If
		If File.Exists(appdir + "\space.exe") <> True Then
			MsgBox("Spacebase Directory Not Found. Please Check the path again!")
			Return False
			Exit Function
		End If
		
		If Directory.Exists(appdir + "\PatchBackup") = false Then
			MsgBox("Patch Backup Directory Not Found, Rollback Cannot Continue!")
			Return False
			Exit Function
		End If
		
		If Directory.Exists(appdir + "\PatchBackup\v106") = False Then
			MsgBox("Version 1.06 Data Directory not found. Rollback Cannot Continue!")
			Return false
			Exit Function
		End If
		
		Return True
	End Function
	
	
	Public Function RemovePatch(appdir As String) As Boolean
		Dim DeleteDir As Integer = 0
		Dim i As Integer = 0
		

		For i = LBound(NameArray) To UBound(NameArray)
			If NameArray(i,1) <> "SAVEGAME" Then
				if File.Exists(appdir & "\PatchBackup\v106\" & NameArray(i, 0)) = True Then
					If File.Exists(appdir & "\" & NameArray(i,0)) = True Then
						File.Delete(appdir & "\" & NameArray(i,0))
					End If
				End If		
				If NameArray(i,0) = "InstalledPatches" Then
					If File.Exists(appdir & "\" & NameArray(i,0)) = True Then
						File.Delete(appdir & "\" & NameArray(i,0))
					End If
				End If
				If NameArray(i,0) = "COPYING" Then
					If File.Exists(appdir & "\" & NameArray(i,0)) = True Then
						File.Delete(appdir & "\" & NameArray(i,0))
					End If
				End If
				If NameArray(i,0) = "README" Then
					If File.Exists(appdir & "\" & NameArray(i,0)) = True Then
						File.Delete(appdir & "\" & NameArray(i,0))
					End If
				End If
			Else
				If File.Exists(My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & NameArray(i,0)) Then
					File.delete(My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & NameArray(i,0))
				End If
			End if
		Next i	
		

		Return True
	End Function
	
	
	Public Function RollbackFiles (appdir As string) As Boolean
		Dim i As Integer = 0
		Dim resourceManager As New ResourceManager("SpacebaseInstaller.patchfiles", Assembly.GetExecutingAssembly)
		Dim result As Byte()
		
		For i = LBound(NameArray) To UBound(NameArray)
			If NameArray(i,1) <> "SAVEGAME" Then
				If File.Exists(appdir & "\PatchBackup\v106\" & NameArray(i,0)) Then
					File.Move(appdir & "\PatchBackup\v106\" & NameArray(i,0), appdir & "\" & NameArray(i,0)) 
				End If
			Else
				'delete savegame
				If File.Exists(My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & NameArray(i,0)) Then
					File.delete(My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & NameArray(i,0))
				End If
				'insert generic save game
				result = resourceManager.getobject(NameArray(i,2))
				File.WriteAllBytes(My.Computer.FileSystem.SpecialDirectories.MyDocuments & "\" & NameArray(i,0), result)
			
			End If 
		Next i
		
		
		directory.Delete(appdir & "\PatchBackup", True)
		
		Return true
	End Function
End Module
