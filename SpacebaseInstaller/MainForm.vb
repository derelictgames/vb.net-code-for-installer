﻿'
' Created by SharpDevelop.
' User: Rick Jones "Skenners"
' Date: 26/09/2015
' Time: 4:31 PM
'
' Last Update: 16/03/2016 by Rick Jones
' 
' 
' To change this template use Tools | Options | Coding | Edit Standard Headers.
'
Imports System.IO

Public Partial Class MainForm
	
	Dim version As String = "1.08.1"
	
	Public Sub New()
		' The Me.InitializeComponent call is required for Windows Forms designer support.
		Me.InitializeComponent()
		textBox1.Text = "C:\Program Files (x86)\steam\steamapps\common\SpacebaseDF9"
		
		label3.Text = "Welcome to the Installer for the" & vbCrLf &	"SpaceBase DF9 " & vbCrLf & "Unofficial Patch " & version
		
		'
		' TODO : Add constructor code after InitializeComponents
		'
	End Sub
	
	Sub Button1Click(sender As Object, e As EventArgs)
		Form1.Show()
	End Sub
	
	Sub ChkTandCCheckedChanged(sender As Object, e As EventArgs)
		If ChkTandC.Checked = True Then
			btnInstall.Enabled = true
		End If
		If ChkTandC.Checked = False Then
			btnInstall.Enabled = False
		End If
	End Sub
	
	Sub BtnInstallClick(sender As Object, e As EventArgs)
		If textBox1.Text <> "" Then
			If processfiles.PatchPrelim(textBox1.Text) = False Then
				MsgBox("File Backup Failure. Please Reinstall Patch")
				Exit Sub
			Else
				progressbar.Value = 25
			End If
			
				If processfiles.RunFileBackup(textBox1.Text) = False Then
					MsgBox("File Backup Failure. Please Reinstall Patch")
					Exit Sub
				End If
			progressBar.Value = 50
			
			If processfiles.CopyPatch(textBox1.Text) = False Then
				MsgBox("File Backup Failure. Please Reinstall Patch")
				Exit Sub
			Else
				progressBar.Value = 75
			End If			
			ProgressBar.value = 100
			MsgBox ("Files Updated! We hope you enjoy the game!")
			ProgressBar.value = 0
		Else
			MsgBox("Please add a directory path to your Installation")
			exit sub
		End If
		
	End Sub
	
	Sub BtnRollbackClick(sender As Object, e As EventArgs)
		If textBox1.Text <> "" Then
			If processfiles.RollbackPrelim(textBox1.Text) = False Then
				Exit Sub
			Else
				progressbar.Value = 25
			End If
			
			If processfiles.RemovePatch(textBox1.Text) = False Then
				Exit Sub
			Else
				progressBar.Value = 50
			End If
			
			If processfiles.RollbackFiles(textBox1.Text) = False Then
				Exit Sub
			Else
				progressBar.Value = 75
			End If			
			ProgressBar.value = 100
			MsgBox ("Files Updated! Patch Removed and v1.06 has been restored!")
			ProgressBar.value = 0
		Else
			MsgBox("Please add a directory path to your Installation")
			exit sub
		End If
	End Sub
	
	Sub BtnOpenClick(sender As Object, e As EventArgs)
		Dim apppath As String
		apppath = ""
	    Dim dialog As New FolderBrowserDialog()
	    dialog.RootFolder = Environment.SpecialFolder.Desktop
	    dialog.SelectedPath = textBox1.text
	    dialog.Description = "Select File Path"
	    If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
	        apppath = dialog.SelectedPath
	    End If
	    textbox1.Text = apppath
	End Sub
	
	Sub BtnReadMeClick(sender As Object, e As EventArgs)
		frmReadMe.Show()		
	End Sub
End Class